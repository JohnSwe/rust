#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

// 'Re-exporting' Names with 'pub use'
// This allows external code to use the public 'hosting' module
pub use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}


// Using External Packages
// Add the rand package to Cargo.toml:
/* file: Cargo.toml
[dependencies]
rand = "0.5.5"
*/
// Now with the 'use' keyword, include rand in our scope:
use rand::Rng;

fn main() {
    let secret_number = rand::thread_rng().gen_range(1, 101);
}

//The standard library, 'std', is shipped with rust; we don't have to add it to Cargo.toml:
use std::collections::HashMap;


// Using nested paths to clean up large `use` lists
// This...
/*
use std::cmp::Ordering;
use std::io;
...can be compressed into: */
use std::{cmp::Ordering, io};

/* turn this...
use std::io;
use std::io::Write;
into...
use std::io::{self, Write};
*/

// If we want to bring all of a path's public items into scope, we can use the Glob Operator:
use std::collections::*;
