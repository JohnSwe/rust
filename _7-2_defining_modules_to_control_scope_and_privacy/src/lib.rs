#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

mod front_of_house {
    // 'front_of_house' is the 'parent' of 'hosting'
    // 'hosting' is a 'child' of 'front_of_house'
    mod hosting {
        fn add_to_waitlist() {}

        fn seat_at_table() {}
        // 'add_to_waitlist' and 'seat_at_table' are sibling modules
    }

    mod serving {
        fn take_order() {}

        fn server_order() {}

        fn take_payment() {}
    }
}