fn main() {
    /* Illustrating the problem of keeping track of indices of a word that can change:
    let mut s = String::from("hello world");

    let word = first_word(&s);

    println!("{}", s);
    println!("{}", word);
    s.clear();
    println!("{}", s);
    println!("{}", word);
    */

    // Using string slices to resolve this problem:
    let mut s = String::from("hello world!");
    let hello = &s[..5];
    let world = &s[6..];

    println!("{}", s);
    println!("{}", hello);
    println!("{}", world);

    // Using the new first_word function; this is an immutable borrow
    let word = first_word(&s);

    //this will not compile! It is is a mutable borrow, which would be fine if we didn't use
    //    the immutable borrow after it (printing the 'word' variable)
    //          1) immutable borrow - ok
    //          2) mutable borrow - still ok
    //          3) immutable borrow used - not ok... immutable borrowers expect the value to remain consistent
    //s.clear();

    //Here is where we use the immutably borrow. It won't compile if the mutable borrow above
    //    is uncommented.
    println!("first word: {}", word);

}

//Using &str as a paramater makes our function more general because it can accept:
//      slices of Strings
//      slices of &str
//      raws &str (because they're already slices!
fn first_word(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (index, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..index];
        }
    }
    &s[..]
}


fn old_first_word(s: &String) -> usize {
    let bytes = s.as_bytes();

    for (index, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return index;
        }
    }

    s.len()
}
