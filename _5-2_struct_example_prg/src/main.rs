struct Rectangle {
    width: i32,
    height: i32,
}

fn main() {
    // Naive program: the width and height of the rectangle are related but there's no way of
    //      knowing that with the function we've written
    let width1 = 30;
    let height1 = 50;
    println!(
        "The area of the rectangle is {} square pixels.",
        naive_area(width1, height1)
    );


    //Refactoring with tuples:
    let rect1: (i32, i32) = (30, 50);
    println!(
        "The area of the rectangle is {} square pixels.",
        tuple_area(rect1)
    );
    /* Pros:
            This is better because we're handling only one entity for the rectange.
       Cons:
            This isn't yet ideal because the elements of the tuple don't have proper names.
            If the order of the width and height mattered, this could be a problem
            How can we fix this... with structs!!
    */


    //Refactoring with structs
    //      We need to initialize the struct above the main so we can use it in the function below
    let rect2: Rectangle = Rectangle {
        width: 30,
        height: 50,
    };
    println!(
        "The area of the rectangle is {} sqaure pixels.",
        struct_area(rect2)
    );

}


fn naive_area(w: i32, h: i32) -> i32 {
    w * h
}

fn tuple_area(dimensions: (i32, i32)) -> i32 {
    dimensions.0 * dimensions.1
}

fn struct_area(rect: Rectangle) -> i32 {
    rect.width * rect.height
}