#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

mod front_of_house {
    // 'front_of_house' is the 'parent' of 'hosting'
    // 'hosting' is a 'child' of 'front_of_house'

    //The default model for Rust accessibility from models:
    //  All modules are private
    //    children can access their ancestor module. They can know the scope in which they exist
    //    parents cannot access their children/sub-modules
    //  We can add the 'pub' keyword to modules to make them accessible to their ancestors, as seen below:
    pub mod hosting {
        pub fn add_to_waitlist() {}
        fn seat_at_table() {}
    }

    mod serving {
        fn take_order() {}
        fn server_order() {}
        fn take_payment() {}
    }
}

fn serve_order() {}

mod back_of_house {
    fn fix_incorrect_order() {
        cook_order();
        super::serve_order();  //Use 'super' to construct relative paths that begin in the parent module
    }

    fn cook_order() {}

    #[derive(Debug)]
    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String,
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast {
                toast: String::from(toast),                 // customer can choose toast
                seasonal_fruit: String::from("peaches"),    // customer cannot choose the fruit, depends on the time of year
            }
        }
    }

    pub enum Appetizer {
        Soup,
        Salad,
    }
}


pub fn eat_at_restaurant() {
    //Absolute path
    //Note that front_of_house is private, but we can access from 'eat_at_restaurant'
    //    because they are siblings in scope
    crate::front_of_house::hosting::add_to_waitlist();
    //Relative path
    front_of_house::hosting::add_to_waitlist();


    // Order a breakfast in the summer with Tye toast
    let mut meal = back_of_house::Breakfast::summer("Rye");
    println!("Our breakfast this morning consists of {:?}", meal);

    // Let's change our toast order because we're indecisive and rude:
    meal.toast = String::from("Sour dough");
    println!("Hey can I go ahead and switch my toast out to {}", meal.toast);

    // The following change to our fruit order will not work because it's private; we can't control what fruit we get with our breakfast
    // meal.seasonal_fruit = String::from("Grapefruit");


    // We had to have a public function to access the Breakfast struct because it has a private member field
    //     Public structs do not pass their public-ness to their fields
    // However, an enum is public, all of its variants are public
    //     Below, we see a use of a public enum, Appetizer
    let order1 = back_of_house::Appetizer::Soup;
    let order2 = back_of_house::Appetizer::Salad;
}


// We don't have to write out the full path to an entity within a module with the 'use' keyword
//     Absolute path with the 'use' keyword
use crate::front_of_house::hosting;
//     Relative path with the 'use' keyword
//use self::front_of_house::hosting;    [commented out so hosting isn't declared twice]

pub fn short_path_eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}
// We also could have drilled down all the way to the 'add_to_waitlist()' function with the 'use' keyword
//     and called 'add_to_waitlist' without its parent.
// However, this is not idiomatic of the 'use' keyword.
//     We use the parent::function structure so it is apparent that the function is not locally defined


// But when it comes to structs, enums, and other items, it's idiomatic to specify the full path with the 'use' keyword
use std::collections::HashMap;
fn main() {
    let mut map = HashMap::new();
    map.insert(1, 2);
}
//The exception to the idiom is bringing in two items with the same name into the same scope
use std::fmt;
use std::io;

fn function1() -> fmt::Result {
    Ok(())
}
fn function2() -> io::Result<()> {
    Ok(())
}

// One trick to resolve this while maintaining our precious ~idiomatic~ usage
//     is the 'as' keyword:
use std::fmt::Result;
use std::io::Result as IoResult;

fn function3() -> Result {
    Ok(())
}
fn function4() -> IoResult<()> {
    Ok(())
}

