//Coins Example
#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
    Arizona,
    Arkansas,
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => {
            println!("Lucky penny!");
            1
        }
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("We got an {:#?} quarter!!", state);
            25
        },
    }
}


//Matching with Option<T>
fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i+1), //bind i to the value that matches in x; i.e. 'Some(5)'
    }
}


fn main() {
    //using the plus_one function to handle Option types
    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);
    //println!("{}, {}, {}", five, six, none);


    //Matches must be exhaustive! We can use the _ placeholder as a catch-all last arm:
    let some_u8_value: u8 = 0;
    match some_u8_value {
        1 => println!("one"),
        3 => println!("three"),
        5 => println!("five"),
        7 => println!("seven"),
        _ => (),
    }
}
