//define a struct:
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn main() {
    //creating an instance of the struct:
    let mut user1 = User {
        username: String::from("someone@example.com"),
        email: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    //change an instance's field wth dot notation:
    user1.email = String::from("different@another.com");
    println!("{}", user1.email);

    //use the buildUser function to create a user:
    let mut user2: User = build_user(String::from("john.sweeney@fraud.io"), String::from("jimbojombo99"));
    println!("{} {}", user2.username, user2.email);

    //use 'struct update syntax' to fill a new user's un-explicitly-initialized fields with
    //    the fields from another user
    let mut user3: User = User {
        username: String::from("billybob09"),
        email: String::from("william.van.williamson@oldmoney.net"),
        ..user2
    };

    println!("{} {}", user3.username, user3.email);


    //'tuple structs':
    //     structs with nameless field
    struct Color(i32, i32, i32);
    struct Point(i32, i32, i32);

    let black = Color(0, 0, 0);
    let origin = Point(0, 0, 0);
}

fn build_user(email: String, username: String) -> User {
    //use awesome short-hand syntax for fields whose names match parameter names
    //    this is called 'field init shorthand'
    User {
        username,
        email,
        active: true,
        sign_in_count: 1,
    }
}
