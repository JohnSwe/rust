#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
    Arizona,
    Arkansas,
    California,
    Iowa,
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn main() {
    // Consider the following program that only matches on a specific case of Option<u8>
    let some_u8_value = Some(0u8);
    match some_u8_value {
        Some(3) => println!("three!"),
        _ => (),
    }

    // We can make this more concise by using `if let`:
    if let Some(3) = some_u8_value {
        println!("three!");
    }


    // Consider a program which announces the States of Quarters and keeps a count of non-quarters:
    //    We could write it with a match operator:
    let coin = Coin::Quarter(UsState::Iowa);
    let mut count: i32 = 0;
    match coin {
        Coin::Quarter(state) => println!("We have a {:?} quarter!!", state),
        _ => count += 1,
    }

    //   Or we could use let if with an else statement:
    let coin2 = Coin::Quarter(UsState::Arizona);
    let mut count: i32 = 0;
    if let Coin::Quarter(state) = coin2 {
        println!("We have a {:?} quarter!!", state)
    } else {
        count += 1;
    }

}
