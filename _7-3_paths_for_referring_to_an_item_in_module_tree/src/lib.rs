#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}


mod front_of_house {
    // 'front_of_house' is the 'parent' of 'hosting'
    // 'hosting' is a 'child' of 'front_of_house'

    //The default model for Rust accessibility from models:
    //  All modules are private
    //    children can access their ancestor module. They can know the scope in which they exist
    //    parents cannot access their children/sub-modules
    //  We can add the 'pub' keyword to modules to make them accessible to their ancestors, as seen below:
    pub mod hosting {
        pub fn add_to_waitlist() {}

        fn seat_at_table() {}
    }

    mod serving {
        fn take_order() {}

        fn server_order() {}

        fn take_payment() {}
    }
}

fn serve_order() {}

mod back_of_house {
    fn fix_incorrect_order() {
        cook_order();
        //Use 'super' to construct relative paths that begin in the parent module
        super::serve_order();
    }

    fn cook_order() {}

    #[derive(Debug)]
    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String,
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast {
                toast: String::from(toast),                 // customer can choose toast
                seasonal_fruit: String::from("peaches"),    // customer cannot choose the fruit, depends on the time of year
            }
        }
    }

    pub enum Appetizer {
        Soup,
        Salad,
    }
}


pub fn eat_at_restaurant() {
    //Absolute path
    //Note that front_of_house is private, but we can access from 'eat_at_restaurant'
    //    because they are siblings in scope
    crate::front_of_house::hosting::add_to_waitlist();
    //Relative path
    front_of_house::hosting::add_to_waitlist();


    // Order a breakfast in the summer with Tye toast
    let mut meal = back_of_house::Breakfast::summer("Rye");
    println!("Our breakfast this morning consists of {:?}", meal);

    // Let's change our toast order because we're indecisive and rude:
    meal.toast = String::from("Sour dough");
    println!("Hey can I go ahead and switch my toast out to {}", meal.toast);

    // The following change to our fruit order will not work because it's private; we can't control what fruit we get with our breakfast
    // meal.seasonal_fruit = String::from("Grapefruit");


    // We had to have a public function to access the Breakfast struct because it has a private member field
    //     Public structs do not pass their public-ness to their fields
    // However, an enum is public, all of its variants are public
    //     Below, we see a use of a public enum, Appetizer
    let order1 = back_of_house::Appetizer::Soup;
    let order2 = back_of_house::Appetizer::Salad;
}
