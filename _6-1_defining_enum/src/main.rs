enum IpAddrKind {
    V4,
    V6,
}

//we can create enums with different data types
enum DiffIpAddrKind {
    V4(u8, u8, u8, u8),
    V6(String),
}

enum Message {
    Quit,
    Move { x: i32, y: i32},
    Write(String),
    ChangeColor(i32, i32, i32),
}

//We can write methods for enums, just like structs:
impl Message {
    fn call(&self) {
        //method body defined here
    }
}

struct IpAddr {
    kind: IpAddrKind,
    address: String,
}

fn main() {
    let four = IpAddrKind::V4;
    let six = IpAddrKind::V6;

    let home = IpAddr {
        kind: IpAddrKind::V4,
        address: String::from("127.0.0.1")
    };

    let loopback = IpAddr {
        kind: IpAddrKind::V6,
        address: String::from("::1")
    };

    //use of enums with different types
    let home2 = DiffIpAddrKind::V4(127, 0, 0, 1);
    let loopback2 = DiffIpAddrKind::V6(String::from("::1"));

    //using the Message enum and method
    let m1: Message = Message::Write(String::from("kentucky shuffle"));
    m1.call();


    //The Option enum is included in the prelude and gives a mechanism of checking whether
    //an item exists or does not exist;
    //    Rust accordingly does not have 'null' types. It pushes users toward using the Option enum
    //    to check if a value exists
    //    The 'Option' enum is defined as follows:
    //          enum Option<t> {
    //              Some(T),
    //              None,
    //          }

    let some_number = Some(5);
    let some_string = Some("a string");

    let absent_number: Option<i32> = None;

    //See _6-2 for information on how to use the Option enum
}
