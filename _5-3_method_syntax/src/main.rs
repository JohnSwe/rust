#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, subj_of_comp: &Rectangle) -> bool {
        if self.width > subj_of_comp.width && self.height > subj_of_comp.height {
            return true;
        } else {
            return false;
        }
    }

    //Associated functions:
    //      don't take &self as a parameter
    fn square(size: u32) -> Rectangle {
        return Rectangle {width: size, height: size};
    }
}

fn main() {
    let rect1 = Rectangle { width: 30, height: 50};

    println!(
        "The area of the rectangle is {} square pixels.",
        rect1.area()
    );

    let rect2 = Rectangle { width: 10, height: 40};
    let rect3 = Rectangle { width: 60, height: 45};

    println!("Can rectangle 1 hold rectangle 2? {}", rect1.can_hold(&rect2));
    println!("Can rectangle 1 hold rectangle 3? {}", rect1.can_hold(&rect3));

    //Use of associated function
    let rect4: Rectangle = Rectangle::square(3);
    println!("{:#?}", rect4)
}
